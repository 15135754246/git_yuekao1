import Head from 'next/head'
import Header from "../components/header"
import styles from "./styles.module.css";
function App() {
  return (
    <div className={styles.App}>
    <Head>
      <title>陆金所控股有限公司</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
<Header></Header>
    <div className={styles.home_index_container}>
    <div id="bannerArea" className={styles.banner_area} style={{height: "762.662px"}}>
    <video  width="100%" src="https://ljs1.wmcn.com/static/upload/video/20201026/16037095810705273.mp4" controls="controls"></video>
    {/* <video width="100%" id="video" x-webkit-airplay="true" airplay="allow" webkit-playsinline="true" playsinline="true" src="https://ljs1.wmcn.com/static/upload/video/20201026/16037095810705273.mp4" className={styles.video1,video} autoplay="" loop="" preload="" muted="" style={{width: "1280px", marginLeft: "-640px"}}></video> */}
    <div className={styles.banner_txt}>
      <img style={{width:"856px",height: "174px;" }}src="https://static.lufaxcdn.com/lufaxholding/pages/lufax/images/home_index_video_title.3688842d.png" alt=""/>
    </div>
    <div className={styles.video_banner_bg}>
    </div>
  </div>
    </div>
  </div>

  );
}

export default App;
