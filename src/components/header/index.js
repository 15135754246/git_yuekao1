import React, { Component } from "react";
import styles from "./styles.module.css";
export default class componentName extends Component {
  render() {
    return (
      <div className={styles.header} id="navHeaader">
      <div className={styles.inner}>
        <div className={styles.logo_wrapper}>
          <a href="/index" id="logo" style={{}}>
            <img
              src="https://static.lufaxcdn.com/lufaxholding/common/images/logo.80d5324a.jpg"
              alt="lufax"
            />
          </a>
          <a href="/index" id="logo_white" style={{ display: "none" }}>
            <img
              src="https://static.lufaxcdn.com/lufaxholding/common/images/logo_white.02384774.jpg"
              alt="lufax"
            />
          </a>
        </div>
        <div className={styles.menus_wrapper}>


        <div className={styles.menus}>
                <div className={styles.menu_item} style={{width:"60px"}} id="switchLanguage">
                    <div className={styles.menu_item_inner}>
                        <div className={styles.lan_switch}>
                            
                            
                                <a href="javascript:void(0)" data-lan="zh-cn" className="">简</a><span>/</span>
                                <a href="javascript:void(0)" data-lan="en-us" className="">EN</a>
                            
                        </div>
                    </div>   
                </div>
                    <div className={styles.menu_item} data-hover="1" data-menu="joinUs" style={{width:"64px"}}>
                        <div className={styles.menu_item_inner} data-hover="1" data-menu="joinUs">
                            <div><a href="https://talent.pingan.com/recruit/index.html" data-hover="1" data-menu="joinUs" className="">加入我们</a></div>
                            <div className={styles.text_decoration_under_line} style={{display: "none"}}></div>
                        </div>   
                    </div>
                <div className={styles.menu_item} style={{width:"64px"}}>
                    <div className={styles.menu_item_inner}>
                        <div>
                            <a href="/contactUs" className="">联系我们</a> 
                        </div>
                        <div className={styles.text_decoration_under_line} style={{display: "none"}}></div>
                    </div>   
                </div>
                
                <div className={styles.menu_item} style={{width:"80px"}}>
                    <div className={styles.menu_item_inner}>
                        <div><a href="https://ir.lufaxholding.com/" className="">投资者关系</a></div>
                        <div className={styles.text_decoration_under_line} style={{display: "none"}}></div>
                    </div>   
                </div>
                
                <div className={styles.menu_item} style={{width:"64px"}}>
                    <div className={styles.menu_item_inner}>
                        <div><a href="/news/list?pageNum=1&amp;tabItem=" className="">新闻中心</a></div>
                        <div className={styles.text_decoration_under_line} style={{display: "none"}}></div>
                    </div>   
                </div>
                <div className={styles.menu_item} data-hover="1" data-menu="services" style={{width:"80px"}}>
                    <div className={styles.menu_item_inner} data-hover="1" data-menu="services">
                        <div><a href="/services" data-hover="1" data-menu="services" className="">业务与服务</a></div>
                        <div className={styles.text_decoration_under_line} style={{display: "none"}}></div>
                    </div>   
                </div>
                <div className={styles.menu_item} data-hover="1" data-menu="aboutUs" style={{width:"64px"}}>
                    <div className={styles.menu_item_inner} data-hover="1" data-menu="aboutUs">
                        <div><a href="/about/index" data-hover="1" data-menu="aboutUs" className="">关于我们</a></div>
                        <div className={styles.text_decoration_under_line} style={{display: "none"}}></div>
                    </div>
                   
                </div>
                <div className={styles.menu_item} style={{width:"32px"}}>
                    <div className={styles.menu_item_inner}>
                        <div><a href="/index" className="">首页</a></div>
                        <div className={styles.text_decoration_under_line} style={{display: "none"}}></div>
                    </div> 
                </div>
            </div>






























        </div>
      </div>
      </div>
    );
  }
}
